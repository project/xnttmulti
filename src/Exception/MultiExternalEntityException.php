<?php

namespace Drupal\xnttmulti\Exception;

/**
 * Exception thrown by External Entities Multiple Storage Client.
 */
class MultiExternalEntityException extends \RuntimeException {}
