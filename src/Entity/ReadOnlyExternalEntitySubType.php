<?php

namespace Drupal\xnttmulti\Entity;

use Drupal\external_entities\Entity\ExternalEntityType;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines a read-only external entity sub type.
 */
class ReadOnlyExternalEntitySubType extends ExternalEntityType {
  
  /**
   * The real external entity type ID.
   *
   * @var string
   */
  protected $parentId;

  /**
   * Creates a external entity sub-type from a parent type.
   *
   * The new instance copies values from a duplicate of the given parent with
   * all its identifiers unset, so saving it would not alter the original
   * parent values.
   *
   * @param ExternalEntityType $parent
   *   An external entity type to clone.
   *
   * @return \Drupal\xnttmulti\Entity\ReadOnlyExternalEntitySubType
   *   A new instance copied form the given parent.
   */
  public static function createFromParent(ExternalEntityType $parent) {
    $values = get_object_vars($parent->createDuplicate());
    $values['id'] = 'xnttmulti_ro_' . $parent->id();
    $values['parentId'] = $parent->id();
    $values['fieldMapperPlugin'] = NULL;
    return new static($values, 'external_entity_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivedEntityTypeId() {
    return $this->parentId;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
  }

}
