<?php

namespace Drupal\xnttmulti\Plugin\ExternalEntities\StorageClient;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\ExternalEntityTypeInterface;
use Drupal\external_entities\Plugin\ExternalEntities\FieldMapper\SimpleFieldMapper;
use Drupal\external_entities\Plugin\PluginFormTrait;
use Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface;
use Drupal\external_entities\StorageClient\ExternalEntityStorageClientBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;
use Drupal\xnttmulti\Exception\MultiExternalEntityException;
use Drupal\xnttmulti\Entity\ReadOnlyExternalEntitySubType;
use Symfony\Component\Yaml\Yaml;

/**
 * External entities multiple storage client.
 *
 * @ExternalEntityStorageClient(
 *   id = "xnttmulti",
 *   label = @Translation("Multiple storages"),
 *   description = @Translation("Generates a combined record from multiple storage sources.")
 * )
 */
class Multi extends ExternalEntityStorageClientBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The external storage client manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $storageClientManager;

  /**
   * Constructs a multiple external storage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface $response_decoder_factory
   *   The response decoder factory service.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $storage_client_manager
   *   The external storage client manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger service (xnttmulti).
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    ResponseDecoderFactoryInterface $response_decoder_factory,
    PluginManagerInterface $storage_client_manager,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $response_decoder_factory
    );
    // Services injection.
    $this->storageClientManager = $storage_client_manager;
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('external_entities.response_decoder_factory'),
      $container->get('plugin.manager.external_entities.storage_client'),
      $container->get('messenger'),
      $container->get('logger.channel.xnttmulti')
    );
  }

  /**
   * Provides the list of available plugins.
   *
   * @param bool $reset
   *   Set to TRUE to reload the list of plugin (clears the static cache).
   *   Default: FALSE (use the static cache).
   *
   * @return array
   *   An array with the following keys containing arrays keyed by plugin ids:
   *   - plugins: an array of storage client plugin objects;
   *   - definitions: an array of plugin descriptions;
   *   - options: an array ready to be used for "radios" form elements.
   */
  protected function getOtherAvailablePlugins(bool $reset = FALSE) {
    static $storage_client_options = [];
    $hash_key = spl_object_hash($this);
    if (empty($storage_client_options[$hash_key]) || $reset) {
      $storage_client_options[$hash_key] = ['options' => [], 'plugins' => [], ];
      // Get all storage definitions to provide a list of storage plugins.
      $all_storage_clients = $this->storageClientManager->getDefinitions();
      foreach ($all_storage_clients as $storage_client_id => $definition) {
        // Exclude this plugin from the list.
        if ($this->getPluginId() == $storage_client_id) {
          continue;
        }
        $config = [
          '_external_entity_type' => $this->externalEntityType,
        ];

        $storage_client = $this->storageClientManager->createInstance(
          $storage_client_id,
          $config
        );
        $storage_client_options[$hash_key]['plugins'][$storage_client_id] =
          $storage_client
        ;
        $storage_client_options[$hash_key]['definitions'][$storage_client_id] =
          $definition
        ;
        $storage_client_options[$hash_key]['options'][$storage_client_id] =
          $storage_client->getLabel()
        ;
      }
      asort($storage_client_options[$hash_key]['options'], SORT_NATURAL | SORT_FLAG_CASE);
      $storage_client_options[$hash_key]['options'] =
        ['' => 'Remove']
        + $storage_client_options[$hash_key]['options']
      ;
    }
    return $storage_client_options[$hash_key];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'storages' => [],
    ];
  }

  /**
   * Initialize externalEntityType member if not set.
   */
  protected function initExternalEntityType(FormStateInterface $form_state) {
    // Get current external entity type if not set.
    if (empty($this->externalEntityType)) {
      $state_data = $form_state->getStorage();
      $xntt_type_id = $state_data['machine_name.initial_values']['id'];
      if (!empty($xntt_type_id)) {
        $xntt_type = \Drupal::entityTypeManager()
          ->getStorage('external_entity_type')
          ->load($xntt_type_id)
        ;
        $this->externalEntityType = $xntt_type;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $this->initExternalEntityType($form_state);

    // Get the list of available storage plugins.
    $storage_clients = $this->getOtherAvailablePlugins();

    $form['storages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multiple storage settings'),
    ];
    $form['storages']['description'] = [
      '#type' => '#markup',
      '#markup' =>
        '<div>'
        . $this->t(
          'You can specify one or more storage client. Their order is important: the first one (of a group) is loaded first and will be used as the reference (for its group). Each group reference is considered holding all the records of the group. Other records provided by the following storage clients will be discarded.'
        )
        . '</div>'
      ,
    ];

    // Get storages from current form or from config.
    $storages =
      $form_state->get('storages')
      ?? $this->configuration['storages']
      ?? []
    ;
    $storage_count = $form_state->get('storage_count') ?? count($storages);
    // We must have at least one storage.
    if (empty($storage_count)) {
      $storage_count = 1;
    }

    $storage_index = 0;
    // Make sure we work on a non-empy list of plugins.
    if (!empty($storage_clients['options'])) {
      for ($i = 0; $i < $storage_count; ++$i) {
        $default_client = $storages[$i]['storage_client'] ?? '';
        // @todo: fix issue when changing plugin: config corresponds to previous
        // plugin instead of the selected one. Maybe check
        // validateConfigurationForm().
        $client_config =
          $storages[$i]['config']
          ?? []
        ;

        // @todo: fix form issue "invalid plugin selection".
        // Issue: Drupal form API will auto-check the wrong radiobox on AJAX
        // reload. If we have 3 storage clients and remove the middle one, the
        // last one will become the "second" item but "Remove" radio will be
        // checked while the displayed settings will correspond to the real
        // last plugin and the 3rd one will remain as an empty plugin but
        // without "Remove" being selected.

        // Only display non-empty clients unless it is the last one.
        if ((!empty($default_client)) || ($i == $storage_count - 1)) {
          if (empty($client_config)
              && !empty($default_client)
              && !empty($this->externalEntityType)
              && ($this->externalEntityType instanceof ExternalEntityTypeInterface)
              && ($this->externalEntityType->getStorageClientId() == $default_client)
          ) {
            $client_config = $this->externalEntityType->getStorageClientConfig();
          }

          $form['storages'][$storage_index] = [
            '#type' => 'details',
            '#title' => $this->t(
              'Storage client #@index',
              ['@index' => $storage_index+1]
            ),
            '#open' => TRUE,
          ];
          $form['storages'][$storage_index]['storage_client'] = [
            '#type' => 'radios',
            '#title' => $this->t('Select a storage client'),
            '#options' => $storage_clients['options'],
            '#default_value' => $default_client,
            // Only require first element.
            '#required' => (0 === $storage_index),
            '#ajax' => [
              'callback' => [
                get_class($form_state->getFormObject()),
                'buildAjaxStorageClientConfigForm',
              ],
              'wrapper' => 'external-entities-storage-client-config-form',
              'method' => 'replace',
              'effect' => 'fade',
            ],
            '#attributes' => ['autocomplete' => 'off',],
          ];

          // Get client config.
          if (!empty($default_client)) {
            $form['storages'][$storage_index]['groups'] = [
              '#type' => 'textfield',
              '#title' => $this->t('Group prefix(es)'),
              '#description' => $this->t('Optional: you can set a group prefix for filtering or group aggregation. Multiple prefixes can be specified separated by semicolons (;).'),
              '#default_value' => implode(';', $storages[$i]['groups'] ?? []),
              '#attributes' => [
                'id' => 'edit-storages-' . $storage_index . '-groups',
              ],
            ];
            $form['storages'][$storage_index]['group_prefix_strip'] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Virtual group prefix'),
              '#description' => $this->t('"Virtual group prefix" means the group prefix is not present on the storage client side but only visible from the External Entity side.'),
              '#default_value' => $storages[$i]['group_prefix_strip'] ?? FALSE,
              '#states' => [
                'invisible' => [
                  ':input[id="edit-storages-' . $storage_index . '-groups"]' => ['value' => ''],
                ],
              ],
            ];

            // The use of override or other identifier field can not work with
            // the first storage client as no field has been loaded for it.
            if (0 !== $storage_index) {
              $form['storages'][$storage_index]['merge_join'] = [
                '#type' => 'textfield',
                '#title' => $this->t(
                  'Previous storage clients field name that provides the identifier value for this storage client (join field)'
                ),
                '#description' => $this->t(
                  'Leave empty to use the default identifier. Otherwise, any (text/numeric) field provided by previous storage clients can be used here as identifier value for this storage client.'
                ),
                '#default_value' => $storages[$i]['merge_join'] ?? NULL,
              ];

              $form['storages'][$storage_index]['id_field'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Source (id) field to use to join data'),
                '#description' => $this->t(
                  'Source field name that will hold the value used to join data with the given previous storage identifier field.'
                ),
                '#default_value' => $storages[$i]['id_field'] ?? '',
              ];

              $form['storages'][$storage_index]['merge'] = [
                '#type' => 'select',
                '#title' => $this->t('How to merge client data'),
                '#options' => [
                  'keep' => $this->t('Keep existing field values (no override)'),
                  'over' => $this->t('Override previous field values'),
                  'sub'  => $this->t('As a sub-object'),
                ],
                '#description' => $this->t(
                  'If set, entity field values provided by this storage client will override existing values with the same field name provided by previous storage clients (except for the identifier and join fields).'
                ),
                '#default_value' => $storages[$i]['merge'] ?? 'keep',
                '#attributes' => [
                  'id' => 'edit-storages-' . $storage_index . '-merge',
                ],
              ];

              $form['storages'][$storage_index]['merge_as_member'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Field name to store sub-object(s) in an array (as read only)'),
                '#description' => $this->t(
                  'Machine name of the field that will hold the array of sub-objects.'
                ),
                '#default_value' => $storages[$i]['merge_as_member'] ?? '',
                '#states' => [
                  'visible' => [
                    ':input[id="edit-storages-' . $storage_index . '-merge"]' => ['value' => 'sub'],
                  ],
                ],
              ];
            }
            else {
              $form['storages'][$storage_index]['id_field'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Source field name to use as identifier for this storage client'),
                '#description' => $this->t(
                  'This field would be used for group prefix filtering and should correspond to the field mapped to the external entity id field. If not set, it will try to use the id mapping unless it is not a simple mapping.'
                ),
                '#default_value' => $storages[$i]['id_field'] ?? NULL,
              ];
            }

            $form['storages'][$storage_index]['readonly'] = [
              '#type' => 'checkbox',
              '#title' => $this->t(
                'Read only (no create/update/delete performed on storage client)'
              ),
              '#default_value' => $storages[$i]['readonly'] ?? FALSE,
            ];

            // Attach the storage client plugin configuration form.
            $form['storages'][$storage_index]['config'] = [];
            $storage_client = $storage_clients['plugins'][$default_client];
            $storage_client->setConfiguration($client_config);

            $storage_client_form_state = SubformState::createForSubform(
              $form['storages'][$storage_index]['config'],
              $form,
              $form_state
            );
            $form['storages'][$storage_index]['config'] =
              $storage_client->buildConfigurationForm(
                $form['storages'][$storage_index]['config'],
                $storage_client_form_state
              )
            ;
            $form['storages'][$storage_index]['config'] += [
              '#type' => 'fieldset',
              '#title' => $this->t(
                ':plugin Settings',
                [':plugin' => $storage_clients['options'][$default_client],]
              ),
              '#open' => TRUE,
            ];
            $description =
              $storage_clients['definitions'][$default_client]['description']
            ;
            if (!empty($description)) {
              $form['storages'][$storage_index]['config'] +=
                ['#description' => $description]
              ;
            }
          }
          else {
            $form['storages'][$storage_index]['config'] = [
              '#type' => 'container',
            ];
          }
          ++$storage_index;
        }
      }
    }
    $storage_count = $storage_index;
    $form_state->set('storage_count', $storage_count);
    $form['storages']['add_storage'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add a storage'),
      '#name' => 'add_storage',
      '#ajax' => [
        'callback' => [
          get_class($form_state->getFormObject()),
          'buildAjaxStorageClientConfigForm'
        ],
        'wrapper' => 'external-entities-storage-client-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Note: there is no ::submitConfigurationForm because External Entities
   * plugin system does not use it. All is done here.
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->initExternalEntityType($form_state);

    $update_count = FALSE;
    // Check for Ajax events.
    if ($trigger = $form_state->getTriggeringElement()) {
      switch ($trigger['#name']) {
        case 'add_storage':
          $storage_count = $form_state->get('storage_count') + 1;
          $form_state->set('storage_count', $storage_count);
          $form_state->setRebuild(TRUE);
          $update_count = TRUE;
          break;

        default:
      }
    }

    // Storages.
    $storages = $form_state->getValue('storages');
    $storage_clients = $this->getOtherAvailablePlugins();
    $storage_client_configs = [];
    $group_prefixes = [];
    foreach ($storages as $storage_index => $storage) {
      // Skip non-client items or empty clients.
      if (!is_int($storage_index) || empty($storage['storage_client'])) {
        continue;
      }
      $storage_client_id = $storage['storage_client'];
      // Get plugin.
      $storage_client = $storage_clients['plugins'][$storage_client_id] ?? NULL;
      if (empty($storage_client)) {
        $update_count = TRUE;
        continue;
      }
      $storage_client_form_state = SubformState::createForSubform(
        $form['storages'][$storage_index]['config'],
        $form,
        $form_state
      );
      $storage_client->validateConfigurationForm(
        $form['storages'][$storage_index]['config'],
        $storage_client_form_state
      );

      $storage_client_config = $storage_client_configs[] = [
        'storage_client'     => $storage_client_id,
        'id_field'           => $storage['id_field'] ?? '',
        'merge'              => $storage['merge'] ?? 'keep',
        'merge_as_member'    => $storage['merge_as_member'] ?? '',
        'merge_join'         => $storage['merge_join'] ?? '',
        'readonly'           => $storage['readonly'] ?? TRUE,
        'groups'             => array_filter(
          preg_split('/\s*;\s*/', trim($storage['groups'] ?? '')),
          'strlen'
        ),
        'group_prefix_strip' => $storage['group_prefix_strip'] ?? FALSE,
        'config'             => $storage_client->getConfiguration(),
      ];
      // Check prefixes...
      // Make sure a given prefix does not appear for the first time in a set.
      $new_prefixes = [];
      foreach ($storage_client_config['groups'] as $prefix) {
        if (!array_key_exists($prefix, $group_prefixes)) {
          $new_prefixes[$prefix] = $prefix;
        }
      }
      if ((!empty($new_prefixes))
          && (count($new_prefixes) != count($storage_client_config['groups']))
      ) {
        $form_state->setErrorByName(
          //'groups',
          'storages][' . (count($storage_client_configs) - 1) . '][groups',
          $this->t(
            'The group prefix(es) %prefix appear(s) for the first time in a set of group prefixes where at least one appeared before. However, when multiple group prefixes are specified, they should ALL appear for the first time or have appeared before.',
            [
              '%prefix' => implode(
                ', ',
                array_diff_key($new_prefixes, $group_prefixes)
              )
            ]
          )
        );
      }
      $group_prefixes += $new_prefixes;

      // Make sure no prefix is part of another one.
      foreach ($new_prefixes as $group_prefix1) {
        foreach ($group_prefixes as $group_prefix2) {
          if (($group_prefix1 != $group_prefix2)) {
            if (strncmp($group_prefix2, $group_prefix1, strlen($group_prefix1)) === 0) {
              $form_state->setErrorByName(
                'storages][' . (count($storage_client_configs) - 1) . '][groups',
                $this->t(
                  'In external entity storage client "Multiple storages", the group prefix %prefix1 is also a prefix for group prefix %prefix2. Prefixes must be strictly distinct.',
                  [
                    '%prefix1' => $group_prefix1,
                    '%prefix2' => $group_prefix2,
                  ]
                )
              );
            }
            elseif (strncmp($group_prefix1, $group_prefix2, strlen($group_prefix2)) === 0) {
              $form_state->setErrorByName(
                'storages][' . (count($storage_client_configs) - 1) . '][groups',
                $this->t(
                  'In external entity storage client "Multiple storages", the group prefix %prefix1 is also a prefix for group prefix %prefix2. Prefixes must be strictly distinct.',
                  [
                    '%prefix1' => $group_prefix2,
                    '%prefix2' => $group_prefix1,
                  ]
                )
              );
            }
          }
        }
      }
    }

    // Update storage count according to "Add storage" button or used "Remove"
    // radios.
    if ($update_count) {
      $form_state->set(
        'storage_count',
        $storage_count ?? count($storage_client_configs)
      );
      $form_state->setRebuild(TRUE);
    }

    //@todo: maybe try to set checked radios here for plugin selection to fix
    // issue "invalid plugin selection".
    // Tryed in $form without success.
    //       $form['storages'][$form_index]['storage_client']['#value'] = $storage_client_id;
    //       $form['storages'][$form_index]['storage_client']['#default_value'] = $storage_client_id;
    //       $form['storages'][$form_index]['storage_client'][$storage_client_id]['#default_value'] = $storage_client_id;
    //       $form['storages'][$form_index]['storage_client'][$storage_client_id]['#checked'] = TRUE;

    $form_state->setValue('storages', $storage_client_configs);
    // Save state for form update when adding items.
    $form_state->set('storages', $storage_client_configs);

    // Only save if there are no errors.
    if (!$form_state->hasAnyErrors()) {
      $this->setConfiguration($form_state->getValues());
    }
  }

  /**
   * Returns the ordered list of configured plugins for current instance.
   */
  public function getPlugins($reload = FALSE) {
    // Static cache.
    static $plugins = [];
    $hash_key = spl_object_hash($this);
    if (!isset($plugins[$hash_key]) || $reload) {
      $plugins[$hash_key] = [];
      $storages = $this->getConfiguration()['storages'] ?? [];
      foreach ($storages as $storage_settings) {
        // Sets externalEntityType member if missing in plugin config.
        if (!empty($this->externalEntityType)
            && ($this->externalEntityType instanceof ExternalEntityTypeInterface)
        ) {
          $source_field_mapper = $this->externalEntityType->getFieldMapper();
          // Create a "read-only" clone of the real external entity type.
          $client_xntt_type = ReadOnlyExternalEntitySubType::createFromParent(
            $this->externalEntityType
          );

          // Create a new dedicated simple field mapper.
          $field_mappings = $source_field_mapper->getFieldMappings();

          // Set "raw" id mapping if needed (ie. not using Drupal field name).
          if (!empty($storage_settings['id_field'])) {
            if (!empty($storage_settings['merge_join'])) {
              $field_mappings[$storage_settings['merge_join']] = [
                'value' => $storage_settings['id_field']
              ];
            }
            else {
              $field_mappings[$storage_settings['id_field']] = [
                'value' => $storage_settings['id_field']
              ];
            }
          }
          else {
            // Check for join fields and set "raw-to-raw" mapping.
            if (!empty($storage_settings['merge_join'])) {
              $field_mappings[$storage_settings['merge_join']] = [
                'value' => $storage_settings['merge_join']
              ];
            }
          }

          $client_xntt_type->setFieldMapperId('simple');
          $client_xntt_type->setFieldMapperConfig([
            'field_mappings' => $field_mappings
          ]);

          $storage_settings['config'] +=
            ['_external_entity_type' => $client_xntt_type,]
          ;
        }

        $plugins[$hash_key][] = $this->storageClientManager->createInstance(
          $storage_settings['storage_client'],
          $storage_settings['config']
        );
      }
    }
    return $plugins[$hash_key];
  }

  /**
   * Filter aggregation plugin.
   *
   * Only return storage plugins that are relevant to the provided entity
   * identifier. Since some plugins may use alternative fields as
   * joins, they will not be filtered here but they should be fitered
   * afterward, from the related join field values.
   *
   * @param $id
   *   Identifier value of current external entity considered.
   */
  protected function filterAggregationPluginsForId(
    $id,
    bool $exclude_read_only = FALSE
  ) {
    $plugins = $this->getPlugins();
    $config = $this->getConfiguration();
    $storage_config = $config['storages'];

    $aggr_plugins = [];
    foreach ($plugins as $plugin_index => $plugin) {
      if ($exclude_read_only
        && !empty($storage_config[$plugin_index]['readonly'])
      ) {
        continue 1;
      }
      $groups = $storage_config[$plugin_index]['groups'] ?? [];
      if (empty($groups)) {
        $aggr_plugins[$plugin_index] = $plugin;
      }
      else {
        $re = '/^\Q' . implode('\E|^\Q', $groups) . '\E/';
        if (1 === preg_match($re, $id)) {
          $aggr_plugins[$plugin_index] = $plugin;
        }
      }
    }

    return $aggr_plugins;
  }

  /**
   * Returns the field name used as identifier.
   */
  public function getIdField($plugin_index = 0) {
    static $id_fields = [];
    $hash_key =
      spl_object_hash($this)
      . $plugin_index
    ;

    if (!isset($id_fields[$hash_key])) {
      $config = $this->getConfiguration();
      $storage_config = $config['storages'];
      if (!empty($storage_config[$plugin_index]['id_field'])) {
        $id_field = $storage_config[$plugin_index]['id_field'];
      }
      else {
        $field_mapper = $this->externalEntityType->getFieldMapper();
        $id_field = current(array_values($field_mapper->getFieldMapping('id')));
      }
      $id_fields[$hash_key] = $id_field;
    }
    return $id_fields[$hash_key];
  }

  /**
   * {@inheritdoc}
   */
  public function delete(ExternalEntityInterface $entity) {
    $storage_config = $this->getConfiguration()['storages'];
    // Filter aggregation plugins.
    $aggr_plugins = $this->filterAggregationPluginsForId($entity->id(), TRUE);

    $original_id = $entity->id();
    foreach ($aggr_plugins as $plugin_index => $plugin) {
      // Check if it is a member object: no edit.
      if (!empty($storage_config[$plugin_index]['merge'])
          && ('sub' == $storage_config[$plugin_index]['merge'])
      ) {
        continue;
      }

      // Set new id to the orignal one to later detect changes.
      $new_id = $original_id;
      // Check if identifier should be adjusted.
      if (!empty($storage_config[$plugin_index]['id_field'])) {
        // Using a join field instead of the default identifier field.
        $raw_data = $entity->toRawData();
        $id_field = $storage_config[$plugin_index]['id_field'];
        $new_id = $raw_data[$id_field] ?? NULL;
      }
      elseif (!empty($storage_config[$plugin_index]['group_prefix_strip'])) {
        // Default identifier should be stipped.
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        // Remove group prefixes.
        $re = '/^\Q' . implode('\E|^\Q', $groups) . '\E/';
        $new_id = preg_replace($re, '', $original_id);
      }

      if (!isset($new_id) || ('' == $new_id)) {
        // The join identifier is not available, skip.
        continue 1;
      }
      if ($new_id === $original_id) {
        // No id change.
        $plugin->delete($entity);
      }
      else {
        // Id change.
        // Delete using the given id.
        $entity->id = $new_id;
        $entity->setOriginalId($new_id);
        $plugin->delete($entity);
        // Put back previous value.
        $entity->id = $original_id;
        $entity->setOriginalId($original_id);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $plugins = $this->getPlugins();
    $config = $this->getConfiguration();
    $storage_config = $config['storages'];

    // Check if we are working with groups.
    if (empty($storage_config[0]['groups'])) {
      // If first storage client has no group, then it is the reference.
      // Only its entities will be taken into account.
      $plugins_by_group = ['' => $plugins];
    }
    else {
      // Otherwise we work by groups.
      // Get each group reference and set of plugins.
      $plugins_by_group = [];
      foreach ($plugins as $plugin_index => $plugin) {
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        if (empty($groups)) {
          // No groups, add plugin to every current group.
          foreach (array_keys($plugins_by_group) as $group) {
            $plugins_by_group[$group][$plugin_index] = $plugin;
          }
        }
        else {
          // Add plugin to each of its groups.
          foreach ($groups as $group) {
            $plugins_by_group[$group][$plugin_index] = $plugin;
          }
        }
      }
    }

    $entities = [];
    foreach ($plugins_by_group as $group => $group_plugins) {
      $group_len = strlen($group);
      // Get the reference storage plugin.
      reset($group_plugins);
      $ref_plugin_number = key($group_plugins);
      $ref_plugin = current($group_plugins);
      // Unset the first plugin to keep indexes but remove it from the set.
      unset($group_plugins[$ref_plugin_number]);
      $id_field = $this->getIdField();
      // Check if the reference plugin uses group filtering.
      $groups = $storage_config[$ref_plugin_number]['groups'] ?? [];
      $valid_ids = $ids;
      if (!empty($group)) {
        // Filter requested ids for this group.
        $valid_ids = array_filter(
          $valid_ids,
          function ($id) use ($group_len, $group) {
            return (strncmp($id, $group, $group_len) === 0);
          }
        );
      }
      // Check if group prefix needs to be stripped.
      if (!empty($storage_config[$ref_plugin_number]['group_prefix_strip'])
        && !empty($group)
      ) {
        // Use virtual pefix: strip.
        $valid_ids = array_map(
          function ($x) use ($group_len) {
            return substr($x, $group_len);
          },
          $valid_ids
        );
      }
      if (!empty($valid_ids)) {
        $plugin_entities = $ref_plugin->loadMultiple($valid_ids) ?? [];
      }
      else {
        $plugin_entities = [];
      }
      // Filter ids to only loaded ids.
      $valid_ids = array_keys($plugin_entities);
      if (!empty($storage_config[$ref_plugin_number]['group_prefix_strip'])
        && !empty($group)
      ) {
        // Use virtual pefix: unstrip.
        $valid_ids = array_map(function ($x) use ($group) { return $group . $x;}, $valid_ids);
        $remapped_entities = [];
        foreach ($plugin_entities as $plugin_entity) {
          if (0 < strlen($plugin_entity[$id_field] ?? '')) {
            $plugin_entity[$id_field] = $group . $plugin_entity[$id_field];
            $remapped_entities[$plugin_entity[$id_field]] = $plugin_entity;
          }
        }
        $plugin_entities = $remapped_entities;
      }
      $valid_ids = array_combine($valid_ids, $valid_ids);
      $entities = array_merge($entities, $plugin_entities);

      // Aggregate values from other storage clients.
      foreach ($group_plugins as $plugin_index => $plugin) {
        // Check if the plugin uses group filtering.
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        // Check if the plugin uses join.
        if (!empty($storage_config[$plugin_index]['id_field'])) {
          // Uses join, get corresponding list of ids to load.
          $plugin_id_field = $storage_config[$plugin_index]['id_field'];
          // Keys are entity real id, values are plugin entity ids.
          $plugin_ids = [];
          foreach ($entities as $entity) {
            // Check if a group match is needed.
            if (empty($groups)
              || (0 === strncmp($entity[$id_field], $group, $group_len))
            ) {
              if (isset($entity[$plugin_id_field])) {
                if (!empty($storage_config[$plugin_index]['merge_join'])) {
                  $group_join_field = $storage_config[$plugin_index]['merge_join'];
                  if (isset($entity[$group_join_field])) {
                    $plugin_ids[$entity[$id_field]] = $entity[$group_join_field];
                  }
                }
                else {
                  $plugin_ids[$entity[$id_field]] = $entity[$plugin_id_field];
                }
              }
            }
          }
        }
        else {
          if (!empty($groups)) {
            // No joins but uses group filtering.
            // Keys are entity real id, values are plugin entity ids.
            $plugin_ids = [];
            foreach ($entities as $entity) {
              if (0 === strncmp($entity[$id_field], $group, $group_len)) {
                // Check for prefix stripping.
                if (!empty($storage_config[$plugin_index]['group_prefix_strip'])) {
                  // Remove group prefixes.
                  $stripped_id = preg_replace('/^\Q' . $group . '\E/', '', $entity[$id_field]);
                  $plugin_ids[$entity[$id_field]] = $stripped_id;
                }
                else {
                  $plugin_ids[$entity[$id_field]] = $entity[$id_field];
                }
              }
            }
          }
          else {
            // Default identifier field withou filtering.
            $plugin_ids = $valid_ids;
          }
        }
        // Get storage client entitites.
        // Check if plugin entities will be loaded as member objects and use
        // a join field.
        if (!empty($storage_config[$plugin_index]['merge'])
            && ('sub' == $storage_config[$plugin_index]['merge'])
            && !empty($storage_config[$plugin_index]['merge_join'])
        ) {
          // Load from a join field.
          $parameters = [[
            // Note: the given field to filter is a raw field, not a Drupal one.
            // However, the query filter only supports Drupal fields.
            // To get around that problem, the plugin has been configured with
            // an altered field mapping that maps the raw field name (as a
            // Drupal field name) to itself. See ::getPlugins() for details.
            'field' => $storage_config[$plugin_index]['merge_join'],
            'value' => array_values($plugin_ids),
            'operator' => 'IN',
          ]];
          $plugin_entities = $plugin->query($parameters) ?? [];
        }
        else {
          // Load from ids.
          // Check for a remapped identifier.
          if (!empty($storage_config[$plugin_index]['id_field'])) {
            $parameters = [[
              'field' => $storage_config[$plugin_index]['id_field'],
              'value' => array_values($plugin_ids),
              'operator' => 'IN',
            ]];
            $plugin_entities = $plugin->query($parameters) ?? [];
          }
          else {
            // Regular source id field.
            $plugin_entities = $plugin->loadMultiple(array_values($plugin_ids)) ?? [];
          }
        }

        // Merge them to previous data.
        foreach ($plugin_ids as $source_id => $plugin_id) {
          if (!empty($plugin_entities[$plugin_id])
            || (!empty($storage_config[$plugin_index]['merge'])
                && ('sub' == $storage_config[$plugin_index]['merge'])
                && !empty($storage_config[$plugin_index]['merge_join']))
          ) {
            // We got something to merge, check for override.
            if (empty($storage_config[$plugin_index]['merge'])
                || ('keep' == $storage_config[$plugin_index]['merge'])
            ) {
              // No override of fields already set.
              // @todo: maybe add an option to only override empty strings?
              //   It might be done by removing empty string values from $entity.
              $entities[$source_id] = NestedArray::mergeDeep(
                $plugin_entities[$plugin_id],
                $entities[$source_id]
              );
            }
            elseif ('over' == $storage_config[$plugin_index]['merge']) {
              // Override any field if needed.
              $entities[$source_id] = NestedArray::mergeDeep(
                $entities[$source_id],
                $plugin_entities[$plugin_id]
              );
            }
            elseif ('sub' == $storage_config[$plugin_index]['merge']) {
              // Store corresponding plugin entities as member.
              $member =
                ($storage_config[$plugin_index]['merge_as_member'] ?? '')
                ?: $plugin->getPluginId() . '_' . $plugin_index
              ;
              // Check if we use a join field on plugin entity.
              if (!empty($storage_config[$plugin_index]['merge_join'])) {
                // Yes, we may have more than one entity to join.
                $source_field = $id_field;
                if (!empty($storage_config[$plugin_index]['id_field'])) {
                  // Uses also join on ref entity.
                  $source_field = $storage_config[$plugin_index]['id_field'];
                }
                $entities[$source_id][$member] = array_filter(
                  $plugin_entities,
                  function ($pe) use ($entities, $storage_config, $source_id, $source_field, $plugin_index) {
                    return !empty($entities[$source_id][$source_field])
                      && !empty($pe[$storage_config[$plugin_index]['merge_join']])
                      && ($pe[$storage_config[$plugin_index]['merge_join']] == $entities[$source_id][$source_field])
                    ;
                  }
                );
              }
              else {
                // Nope, add corresponding plugin entities as members.
                $entities[$source_id][$member] = [$plugin_entities[$plugin_id]];
              }
            }
            else {
              // Warn for invalid setting.
            }
          }
        }
      }
    }
    return $entities;
  }

  /**
   * Loads one entity.
   *
   * @param mixed $id
   *   The ID of the entity to load.
   *
   * @return array|null
   *   A raw data array, NULL if no data returned.
   */
  public function load($id) {
    $entities = $this->loadMultiple([$id]);
    return array_key_exists($id, $entities)
      ? $entities[$id]
      : NULL
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    $storage_config = $this->getConfiguration()['storages'];
    // Filter aggregation plugins.
    $aggr_plugins = $this->filterAggregationPluginsForId($entity->id(), TRUE);

    $original_id = $entity->id();
    foreach ($aggr_plugins as $plugin_index => $plugin) {
      // Set new id to the orignal one to later detect changes.
      $new_id = $original_id;
      // Check if identifier should be adjusted.
      if (!empty($storage_config[$plugin_index]['id_field'])) {
        // Using a join field instead of the default identifier field.
        $raw_data = $entity->toRawData();
        $id_field = $storage_config[$plugin_index]['id_field'];
        $new_id = $raw_data[$id_field] ?? NULL;
      }
      elseif (!empty($storage_config[$plugin_index]['group_prefix_strip'])) {
        // Default identifier should be stipped.
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        // Remove group prefixes.
        $re = '/^\Q' . implode('\E|^\Q', $groups) . '\E/';
        $new_id = preg_replace($re, '', $original_id);
      }

      if (!isset($new_id) || ('' == $new_id)) {
        // The join identifier is not available, skip.
        continue 1;
      }
      if ($new_id === $original_id) {
        // No id change.
        $plugin->save($entity);
      }
      else {
        // Id change.
        // Save using the given id.
        $entity->id = $new_id;
        $entity->setOriginalId($new_id);
        $plugin->save($entity);
        // Put back previous value.
        $entity->id = $original_id;
        $entity->setOriginalId($original_id);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query(
    array $parameters = [],
    array $sorts = [],
    $start = NULL,
    $length = NULL,
    bool $reload = FALSE
  ) {
    $entities = [];

    $plugins = $this->getPlugins();
    $config = $this->getConfiguration();
    $storage_config = $config['storages'];

    // Prepare sets of plugins by group.
    if (empty($storage_config[0]['groups'])) {
      // If first storage client has no group, then it is the reference.
      // Only its entities will be taken into account.
      $plugins_by_group = ['' => $plugins];
    }
    else {
      // Otherwise we work by groups.
      // Get each group reference and set of plugins.
      $plugins_by_group = [];
      foreach ($plugins as $plugin_index => $plugin) {
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        if (empty($groups)) {
          // No groups, add plugin to every current group.
          foreach (array_keys($plugins_by_group) as $group) {
            $plugins_by_group[$group][$plugin_index] = $plugin;
          }
        }
        else {
          // Add plugin to each of its groups.
          foreach ($groups as $group) {
            $plugins_by_group[$group][$plugin_index] = $plugin;
          }
        }
      }
    }

    $plugins_by_group_to_use = $plugins_by_group;
    // See if pagger is used.
    $group_start = $start ?? NULL;
    if (isset($start) && (0 < $start)) {
      // Pagger is used. Find where to start.
      while ($plugins_by_group_to_use) {
        reset($plugins_by_group_to_use);
        $group = key($plugins_by_group_to_use);
        $group_plugins = $plugins_by_group_to_use[$group];
        reset($group_plugins);
        $ref_plugin = current($group_plugins);
        //@todo: fix id prefix in parameters.
        $group_count = $ref_plugin->countQuery($parameters);
        if ($group_count > $group_start) {
          // Got the starting group.
          break;
        }
        else {
          // Start somewhere after current group.
          unset($plugins_by_group_to_use[$group]);
          $group_start -= $group_count;
        }
      }
    }

    // Query entities from groups.
    $group_length = NULL;
    $id_field = $this->getIdField();
    while ((count($entities) < ($length ?? INF))
      && $plugins_by_group_to_use
    ) {
      // Get current plugin group.
      reset($plugins_by_group_to_use);
      $group = key($plugins_by_group_to_use);
      $group_plugins = $plugins_by_group_to_use[$group];
      unset($plugins_by_group_to_use[$group]);
      // Get reference plugin.
      reset($group_plugins);
      $ref_plugin_number = key($group_plugins);
      $ref_plugin = current($group_plugins);
      unset($group_plugins[$ref_plugin_number]);

      if (isset($length)) {
        $group_length = $length - count($entities);
      }
      // @todo: we can't check if the returned entity identifiers match their
      // group. Maybe we should add an id filter? The problem should also be
      // considered for the count() method.
      if (!empty($storage_config[$ref_plugin_number]['group_prefix_strip'])
        && !empty($group)
      ) {
        // Strip ids in parameters.
        $group_parameters = [];
        foreach ($parameters as $parameter) {
          if ($parameter['field'] == $id_field) {
            if (is_array($parameter['value'])) {
              $parameter['value'] = array_map(
                function ($x) use ($group) {
                  return preg_replace('/^\Q' . $group . '\E/', '', $x);
                },
                $parameter['value']
              );
            }
            else {
              $parameter['value'] = preg_replace('/^\Q' . $group . '\E/', '', $parameter['value']);
            }
          }
          $group_parameters[] = $parameter;
        }
      }
      else {
        $group_parameters = $parameters;
      }
      $group_entities = $ref_plugin->query($group_parameters, $sorts, $group_start, $group_length, $reload) ?? [];
      if (!empty($storage_config[$ref_plugin_number]['group_prefix_strip'])
        && !empty($group)
      ) {
        // Use virtual pefix: unstrip.
        $group_entities = array_map(
          function ($x) use ($group, $id_field) {
            $x[$id_field] = $group . $x[$id_field];
            return $x;
          },
          $group_entities
        );
      }
      $group_entities = array_combine(
        array_map(
          function ($x) use ($id_field) { return $x[$id_field]; },
          $group_entities
        ),
        $group_entities
      );
      $entities = array_merge($entities, $group_entities);

      if (isset($start)) {
        // If it was set for the first group, now restart from 0 for the nexts.
        $group_start = 0;
      }

      // Disabled because we don't need to aggregate data when using query as
      // only entity identifiers are used and it is not possible to filter on
      // other group storage client values for technical reasons (must match
      // count() method which does not support that as well).
      // // Query other storage clients of the group.
      // $group_ids = array_keys($group_entities);
      // $group_ids = array_combine($ids, $ids);
      // $plugin_id_field = $id_field;
      // foreach ($group_plugins as $plugin_index => $plugin) {
      //   // Check if the plugin uses group filtering.
      //   $groups = $storage_config[$plugin_index]['groups'] ?? [];
      //   // Check if the plugin uses join.
      //   if (!empty($storage_config[$plugin_index]['id_field'])) {
      //     // Uses join, get corresponding list of ids to load.
      //     $plugin_id_field = $storage_config[$plugin_index]['id_field'];
      //     // Keys are entity real id, values are plugin entity ids.
      //     $plugin_ids = [];
      //     foreach ($group_entities as $entity) {
      //       // Check if a group match is needed.
      //       if (empty($groups)
      //         || (0 === strncmp($entity[$id_field], $group, strlen($group)))
      //       ) {
      //         if (isset($entity[$plugin_id_field])) {
      //           $plugin_ids[$entity[$id_field]] = $entity[$plugin_id_field];
      //         }
      //       }
      //     }
      //   }
      //   else {
      //     if (!empty($groups)) {
      //       // No join but uses group filtering.
      //       // Keys are entity real id, values are plugin entity ids.
      //       $plugin_ids = [];
      //       foreach ($group_entities as $entity) {
      //         if (0 === strncmp($entity[$id_field], $group, strlen($group))) {
      //           // Check for prefix stripping.
      //           if (!empty($storage_config[$plugin_index]['group_prefix_strip'])) {
      //             // Remove group prefixes.
      //             $stripped_id = preg_replace('/^\Q' . $group . '\E/', '', $entity[$id_field]);
      //             $plugin_ids[$entity[$id_field]] = $stripped_id;
      //           }
      //           else {
      //             $plugin_ids[$entity[$id_field]] = $entity[$id_field];
      //           }
      //         }
      //       }
      //     }
      //     else {
      //       // Default identifier field withou filtering.
      //       $plugin_ids = $group_ids;
      //     }
      //   }
      //   // Add id parameter.
      //   $group_id_parameter = [
      //     'field' => $plugin_id_field,
      //     'value' => array_values($plugin_ids),
      //     'operator' => 'IN',
      //   ];
      //   // Get next storage client entitites.
      //   $group_parameters = array_merge($parameters, $group_id_parameter);
      //   $plugin_entities = $plugin->query($group_parameters, NULL, NULL, NULL, $reload);
      //   $plugin_entities = array_combine(
      //     array_map(
      //       function ($x) use ($plugin_id_field) { return $x[$plugin_id_field]; },
      //       $plugin_entities
      //     ),
      //     $plugin_entities
      //   );
      //
      //   // Merge them to previous data.
      //   foreach ($plugin_ids as $source_id => $plugin_id) {
      //     if (!empty($plugin_entities[$plugin_id])) {
      //       // We got something to merge, check if override.
      //       if (!empty($storage_config[$plugin_index]['override'])) {
      //       // Override any field if needed.
      //         $entities[$source_id] = NestedArray::mergeDeep(
      //           $entities[$source_id],
      //           $plugin_entities[$plugin_id]
      //         );
      //       }
      //       else {
      //         // No override of fields already set.
      //         // @todo: maybe add an option to only override empty strings?
      //         //   It might be done by removing empty string values from $entity.
      //         $entities[$source_id] = NestedArray::mergeDeep(
      //           $plugin_entities[$plugin_id],
      //           $entities[$source_id]
      //         );
      //       }
      //     }
      //   }
      // }
    }
    return array_values($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function countQuery(array $parameters = []) {
    $count = 0;

    $plugins = $this->getPlugins();
    $config = $this->getConfiguration();
    $storage_config = $config['storages'];

    // Count entities from plugins.
    $count = 0;
    if (empty($storage_config[0]['groups'])) {
      // If first storage client has no group, then it is the reference.
      // Only its entities will be taken into account.
      $count += $plugins[0]->countQuery($parameters);
    }
    else {
      // Otherwise we work by groups.
      // Get each group reference and count.
      $counted_groups = [];
      foreach ($plugins as $plugin_index => $plugin) {
        $groups = $storage_config[$plugin_index]['groups'] ?? [];
        if (empty($groups)) {
          // No groups, skip.
          continue;
        }
        else {
          // Count each new group.
          foreach ($groups as $group) {
            if (empty($counted_groups[$group])) {
              // @todo: we can't check if the counted entity identifiers match
              // their group. Maybe we should add an id filter?
              $count += $plugin->countQuery($parameters);
              $counted_groups[$group] = TRUE;
            }
          }
        }
      }
    }

    return $count;
  }

}
