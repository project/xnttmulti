# External Entities Multiple Storage plugin
-------------------------------------------

Enables the combination of multiple data storage plugin for a same external
entity type.

===============================

## CONTENTS OF THIS FILE
------------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Aggregation
 * Troubleshooting
 * FAQ
 * Maintainers

## INTRODUCTION
---------------

While External Entities module enables the use of data external to Drupal as
Drupal entities, it has initially been designed to work with one external data
source by external entity type. However, there are uses cases when one wants to
combine data from multiple sources into one virtual data source. For instance,
there could be 2 distinct REST services that provide the same type of data and
one would want to manage them through a single external entity type, avoiding
duplicating config (external entity fields, mapping, and so on).
Another example would be some data stored in a database with additional
related data stored into files, likes images or videos that would be too heavy
for a database. In that cas, it would be convenient to have a single external
entity type that combines both data from the database and files.

The External Entities Multiple Storage plugin provides a way to aggregate
multiple external entity storage sources into a single virtual one. It is
therefore possible to configure and use a single external entity type that is
composed by multiple external sources of any type in any order or number.

## REQUIREMENTS
---------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)
 * One or more external entities storage plugins

## INSTALLATION
---------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION
----------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add a new storage client for external entity types.
Then, each external entity type using it as a stroage client will have its own
External Entities Multiple Storage plugin settings (as well as "sub-storage"
settings).

## AGGREGATION
--------------

### Aggregation Modes
---------------------

When combining multiple entities, three use cases are possible:

1. each data source adds properties to the entities of the previous source(s),
  either because the entity data from both sources share the same identifier or
  the value of a specified field of the new data source entity matches the
  value of a specified field from the previous sources (ie. "join").

2. each data source brings its own set of entities. Therefore, entity
  identifiers should all differ from one data source to another. If it is not
  the case, data source entity identifiers could be prefixed by xnttmulti
  using a "virtual group prefix" on a data source-basis making each identifier
  unique (ie. source A as an entity with id '001', source B as a different
  entity that also uses the id '001'; xnttmulti can virtuall prefix A ids with
  'A' and B ids with 'B' which would lead to 2 distinct identifiers: 'A001' and
  'B001' which clearly identifies each source).

3. a mix of the 2 previous cases.

This modules offers different approaches to deal with those 3 cases but with
some restrictions. Those restrictions are here to solve some technical aspects:
 - being able to efficiently count and filter external entities without needing
   to query all the data from all the sources and process all of them each time.
 - being able to clearly identify the corresponding element in each data source
   given an identifier without any ambiguity.

For the first case, this module assumes the first data source holds ALL the
entity identifiers and can be used for pagging management. If an identifier is
not present in the first data source but in one (or more) of the next data
sources, the corresponding entity will be discarded. For filters, only
reference sources are used (at the moment).

For the second and third cases, a "group prefix" must be provided to every set
of data in order to discriminate data source according to the given identifiers.
For instance, if we have 2 data sources A and B, we need to know that the
identifier '001' corresponds to an entity stored in A or B. Therefore, either
one of the 2 sources uses a prefix like '0' (or '00') and can be clearly
identified (which mich be necessary when creating a new entity instance to avoid
duplicating it in the 2 sources) or the sources have been assigned a virtual
group prefix that will not be stored (nor known by the data sources) but only
used by xnttmulti to discreminate between sources. We call it a "group prefix"
because a same prefix could be used by multiple data sources in order to
aggregate them together like in the first use case.

The first data source of each group will be considered as the group "reference"
and processed just like in the first use case for aggregation and pagging
management. When group prefixes are used, any entity from data sources with or
without group prefix will only be considered and aggregated if its identifier or
join field value matches an existing entity from previous sources. Group
prefixes may be virtual (stripped and not comunicated to the data source) or
physical (part of the identifier provided by the data source) but all group
prefixes must be distinct (no overlap): you can't use the prefix 'AB' on one
data source and the prefix 'ABC' on another for a same external entity type
using xnttmulti since 'AB' is also a part of the prefix 'ABC'.

There is a special case when the first(s) data source(s) is (are) not using a
group prefix while other sources have one. Then, xnttmulti will use the first
use case behavior and only use the next specified group as filters on each data
source that has a group.

It is possible to specify more than one group prefix to each data source by
separating them with a semicolon. It might be conveinent when some data sources
are common to other distinct sources. For instance a data source A and a data
source B may contain distinct entities while a data source C may contain
additional data for both; then A would use the prefix group "A", B the prefix
group "B" and C would use "A;B".

If a data source uses multiple *virtual* prefixes (ie. not part of the "real"
identifier), each of its entities will be duplicated for each prefix. For
instance, if a data source C contains an entity '001' and uses the virtual group
prefixes "A;B", xnttmulti will consider that entity '001' has 2 copies: 'A001'
and 'B001' (it virtually doubles the number of entities).

When an entity is aggregated from several data sources (same identifier or with
join), it is possible to choose the way to merge the last data source:
- either without override and only new fields are added
- or with override and any field from the last source will replace previous
  existing values, except for the entity identifier field or its join fields
- or as a sub-object which will be hold in a new user-defined field name not
  existing in the sources.

### Joins
---------

Sometimes, the identifier field has different names on different sources,
sometimes a different field should be used to join a record from a data source
to the corresponding record in another "joined" source.

To manages thoses cases, this module offers 2 setting fields and different
behavior depending on the way they are filled; the first field is "Previous
storage clients field name that provides the identifier value for this storage
client (join field)", which we will call "join field", and the field "Source
(id) field to use to join data", which we will call "source id" that correspond
to a field of the joined source that will be used as id field for that source.

If both "join id" and "source id" are left empty, xnttmulti assumes the default
identifier field must be available on the joined data source and will match it.
If both "join id" and "source id" are filled, xnttmulti will use the field value
of "join id" field from the previous sources as the identifier value to
use to match "source id" field in the new data source.
If "join id" is filled but "source id" is empty, xnttmulti assumes "source id"
field is the same as the one specified for "join id" and proceed just like when
the "join id" and "source id" are filled.
If "join id" is empty but "source id" is filled, xnttmulti will match the
default identifier provided by previous sources with "source id" field against
the new data source (and it will apply prefix virtualization if needed).
Note: when "join id" is filled, the group prefix will not be used to filter data
on the new source; it will only be used to know if the source should be
aggregated to a given group.

### Examples
------------

Let's consider the following examples:

  * Example 1:
  - Data source "alpha":
    - Entity 1: id='A001', field1='abc'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 3: id='X003', field1='ghi'

  - Data source "beta":
    - Entity 1: id='B001', field1='jkl', field2='def'
    - Entity 2: id='B002', field1='mno', field2='ghi', field3='B001', field4='421'

  - Data source "gamma":
    - Entity 1: id='A001', field1='pqr', field2='def'
    - Entity 2: id='B002', field1='stu', field2='fed', field4='806'
    - Entity 3: id='X003', field1='vwx', field2='xyz', field4='421'

  Examples of settings and the corresponding sets of aggregated (final) data:

  - Aggregate all, no group, no override:
    - Entity 1: id='A001', field1='abc'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 3: id='X003', field1='ghi'

  - Aggregate all, no group, with override:
    - Entity 1: id='A001', field1='pqr', field2='def'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 3: id='X003', field1='vwx', field2='xyz', field4='421'

  - Aggregate all, no group, with override, using 'field4' as gamma identifier:

    - Entity 1: id='A001', field1='abc'
    - Entity 2: id='A002', field1='stu', field2='fed', field4='806'
    - Entity 3: id='X003', field1='ghi'

  - Aggregate by group prefix alpha='A', beta='B', gamma=none, no override:
    - Entity 1: id='A001', field1='abc', field2='def'
    - Entity 2: id='A002', field1='def', field4='421'
    - Entity 3: id='B001', field1='jkl', field2='def'
    - Entity 4: id='B002', field1='mno', field2='ghi', field3='B001', field4='806'
    
    - Entity 1: id='A001', field1='abc', field2='def'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 4: id='B001', field1='jkl', field2='def'
    - Entity 5: id='B002', field1='mno', field2='ghi', field3='B001', field4='421'

  - Aggregate by group prefix alpha='A,X', beta='B', gamma=none, no override:
    - Entity 1: id='A001', field1='abc', field2='def'
    - Entity 2: id='A002', field1='def', field4='421'
    - Entity 3: id='X003', field1='ghi', field2='xyz'
    - Entity 4: id='B001', field1='jkl', field2='def'
    - Entity 5: id='B002', field1='mno', field2='ghi', field3='B001', field4='806'

    - Entity 1: id='A001', field1='abc', field2='def'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 3: id='X003', field1='ghi', field2='xyz', field4='421'
    - Entity 4: id='B001', field1='jkl', field2='def'
    - Entity 5: id='B002', field1='mno', field2='ghi', field3='B001', field4='421'

  - Aggregate by group prefix alpha='A', beta='B', gamma='B,X', with override:
    - Entity 1: id='A001', field1='abc'
    - Entity 2: id='A002', field1='def', field4='421'
    - Entity 3: id='X003', field1='vwx', field2='xyz'
    - Entity 4: id='B001', field1='jkl', field2='def'
    - Entity 5: id='B002', field1='stu', field2='fed', field3='B001', field4='806'

    - Entity 1: id='A001', field1='pqr', field2='def'
    - Entity 2: id='A002', field1='def', field4='806'
    - Entity 3: id='X003', field1='vwx', field2='xyz', field4='421'
    - Entity 4: id='B001', field1='jkl', field2='def'
    - Entity 5: id='B002', field1='stu', field2='fed', field3='B001', field4='806'

  * Example 2:
  - Data source "alpha":
    - Entity 1: id='001', field1='abc'
    - Entity 2: id='002', field1='def', field4='421'
    - Entity 3: id='003', field1='ghi'

  - Data source "beta":
    - Entity 1: id='001', field1='jkl', field2='def'
    - Entity 2: id='002', field1='mno', field2='ghi', field3='001', field4='806'

  - Data source "gamma":
    - Entity 1: id='001', field1='pqr', field2='def'
    - Entity 2: id='002', field1='stu', field2='fed'
    - Entity 3: id='003', field1='vwx', field2='xyz'

  Examples of settings and the corresponding sets of aggregated (final) data:

  - Aggregate by group prefix alpha='a', beta='b', gamma=none, no override, with virtual prefix:
    - Entity 1: id='a001', field1='abc'
    - Entity 2: id='a002', field1='def', field4='421'
    - Entity 3: id='a003', field1='ghi'
    - Entity 4: id='b001', field1='jkl', field2='def'
    - Entity 5: id='b002', field1='mno', field2='ghi', field3='001', field4='806'

  - Aggregate by group prefix alpha='a', beta='b', gamma='a,b', with override, with virtual prefix:
    - Entity 1: id='a001', field1='pqr', field2='def'
    - Entity 2: id='a002', field1='stu', field2='fed', field4='806'
    - Entity 3: id='a003', field1='vwx', field2='xyz', field4='421'
    - Entity 4: id='b001', field1='pqr', field2='def'
    - Entity 5: id='b002', field1='stu', field2='fed', field3='001', field4='806'
    
  - Aggregate by group prefix alpha='a', beta='b', gamma='b,x', with override, with virtual prefix:

    - Entity 1: id='a001', field1='abc'
    - Entity 2: id='a002', field1='def', field4='421'
    - Entity 3: id='a003', field1='ghi'
    - Entity 4: id='b001', field1='pqr', field2='def'
    - Entity 5: id='b002', field1='stu', field2='fed', field3='001', field4='806'
    - Entity 6: id='x001', field1='pqr', field2='def'
    - Entity 7: id='x002', field1='stu', field2='fed', field4='806'
    - Entity 8: id='x003', field1='vwx', field2='xyz', field4='421'

  - Aggregate by group prefix alpha=none, beta='002', gamma=none, with override, using 'field4' as gamma identifier (no virtual identifier):
    - Entity 1: id='001', field1='abc'
    - Entity 2: id='002', field1='vwx', field2='xyz', field4='421'
    - Entity 3: id='003', field1='ghi'
    - Entity 5: id='002', field1='stu', field2='fed', field3='001', field4='806'

TROUBLESHOOTING
---------------

 * If:

   - Are ?

   - Does ?

FAQ
---

Q: I ?

A: Yes.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
